"""
Main URL mapping configuration file.

Include other URLConfs from external apps using method `include()`.

It is also a good practice to keep a single URL to the root index page.

This examples uses Django's default media
files serving technique in development.
"""

from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from rest.views import my_view
from server.product.views import ProductViewSet
from server.producttype.views import ProductTypeViewSet

admin.autodiscover()

router = DefaultRouter()
router.register(r'product', ProductViewSet)
router.register(r'producttype', ProductTypeViewSet)

urlpatterns = [
    # Apps:
    path('', my_view),
    path('api/', include(router.urls)),

    # django-admin:
    path('admin/', admin.site.urls),

    # djoser:
    path('api/', include('djoser.urls')),
    path('api/', include('djoser.urls.authtoken')),
]

if settings.DEBUG:  # pragma: no cover
    from django.conf.urls.static import static  # noqa: WPS433

    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT,
    )
    urlpatterns += static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT,
    )
