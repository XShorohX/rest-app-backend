FROM python:3.9

ARG DJANGO_ENV

ENV DJANGO_ENV=${DJANGO_ENV} \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PYTHONDONTWRITEBYTECODE=1 \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 


WORKDIR /code

COPY . .

# Installation poetry
RUN pip3 install poetry
RUN poetry export -f requirements.txt --output requirements.txt
# Disable virtualenvs
#RUN poetry config virtualenvs.create false --local
# Install dependencies
#RUN poetry install --no-dev
RUN pip install -r requirements.txt
# Staticfiles collect
# RUN python manage.py collectstatic