from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from server.product.models import ProductModel, ProductTagModel


class ProductModelAdmin(admin.StackedInline):
    """StackedInline for meetup's ProductModel."""

    model = ProductTagModel
    

@admin.register(ProductModel)
class PostAdmin(SimpleHistoryAdmin):
    """Register MeetModel in Django admin panel."""

    inlines = [ProductModelAdmin]
    list_display = [
        'name',
        'product_type',
    ]
    search_fields = [
        'product_type',
    ]
    history_list_display = [
        'price',
        'img_url',
        'description',
        'product_type',
    ]

    class Meta(object):
        """Metaclass for the PostAdmin."""

        model = ProductModel
