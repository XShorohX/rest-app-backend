from concurrency.fields import IntegerVersionField
from concurrency.forms import VersionWidget
from django.db.models import Model
from django.db.models.deletion import CASCADE, SET_NULL
from django.db.models.fields import CharField, IntegerField, TextField
from django.db.models.fields.files import ImageField
from django.db.models.fields.related import ForeignKey
from django.urls import reverse
from simple_history.models import HistoricalRecords

from server.producttype.models import ProductTypeModel

MAX_LENGTH = 255


class ProductModel(Model):
    """Model ProductModel."""

    version = IntegerVersionField(widget=VersionWidget)
    name = CharField(verbose_name='Название', max_length=MAX_LENGTH)
    price = IntegerField(verbose_name='Цена',)
    img_url = ImageField(
        verbose_name='Картинка',
        upload_to='static/',
        blank=True,
        null=True,
    )
    description = TextField(verbose_name='Описание', blank=True)
    product_type = ForeignKey(
        ProductTypeModel,
        verbose_name='Тип',
        on_delete=SET_NULL,
        null=True,
        blank=True,
    )
    history = HistoricalRecords(cascade_delete_history=True)

    class Meta(object):
        """Metaclass for the ProductModel."""

        db_table = 'product'
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'
        ordering = ('name',)

    def __str__(self):
        """This is the __str__ for the ProductModel."""
        return str(self.name) if self.name is not None else 'Нет продукта'

    def get_absolute_url(self):
        """This is the get_absolute_url for the ProductModel."""
        return reverse('productmodel-detail', kwargs={'pk': self.pk})


class ProductTagModel(Model):
    """ProductTagModel model."""

    product = ForeignKey(ProductModel, related_name='tags', on_delete=CASCADE)
    tag = CharField(verbose_name='Тэг', max_length=MAX_LENGTH, blank=True)

    class Meta(object):
        """Metaclass for the ProductTagModel."""

        db_table = 'product_tag'
        verbose_name = 'Тэг продукта'
        verbose_name_plural = 'Тэги продуктов'
        ordering = ('product',)

    def __str__(self):
        """This is the __str__ for the ProductTagModel."""
        return str(self.tag) if self.tag is not None else 'Нет тега продукта'
