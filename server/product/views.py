from concurrency.api import disable_concurrency
from concurrency.views import ConflictResponse
from diff_match_patch import diff_match_patch
from django.core.exceptions import ObjectDoesNotExist
from django.template import loader
from django.utils.safestring import mark_safe
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.serializers import SerializerMethodField

from server.product.models import ProductModel
from server.product.serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    """View for ProductModel."""

    serializer_class = ProductSerializer
    queryset = ProductModel.objects.all()
    filterset_fields = ['product_type']

    @action(detail=True, methods=['get'])
    def get_history_all(self, request, pk=None):
        product = self.get_object()
        serializer = SerializerMethodField()
        serializer.history = product.history.all().values(
            'id',
            'name',
            'description',
            'img_url',
            'price',
            'product_type',
            'product_type_id',
            'history_change_reason',
            'history_date',
            'history_user',
            'history_id'
        )
        return Response(serializer.history)
    
    @action(detail=True, methods=['get','put'])
    def reverse_save(self, request, pk=None):
        reverse_id=request.GET.get("reverse_id", "")
        product = self.get_object()
        history = product.history.all().get(history_id=reverse_id)
        with disable_concurrency():
            return Response(history.instance.save())

def get_diff(current, stored):
    data = []
    dmp = diff_match_patch()
    fields = current._meta.fields
    for field in fields:
        v1 = getattr(current, field.name, "")
        v2 = getattr(stored, field.name, "")
        diff = dmp.diff_main(str(v1), str(v2))
        dmp.diff_cleanupSemantic(diff)
        html = dmp.diff_prettyHtml(diff)
        html = mark_safe(html)
        data.append((field, v1, v2, html))
    return data

def conflict(request, target=None, template_name='409.html'):
        template = loader.get_template(template_name)
        try:
            saved = target.__class__._default_manager.get(pk=target.pk)
            diff = get_diff(target, saved)
        except ObjectDoesNotExist:
            saved = None
            diff = None

        ctx = {
            'target': target,
            'diff': diff,
            'saved': saved,
            'request_path': request.path
            }
        return ConflictResponse(template.render(ctx))