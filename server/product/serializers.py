from rest_framework.serializers import (
    CharField,
    ListSerializer,
    ModelSerializer,
)

from server.product.models import ProductModel, ProductTagModel


class ProductTagModelSerializer(ModelSerializer):
    """Serializer for ProductTagModel."""

    class Meta(object):
        model = ProductTagModel
        fields = [
            'tag',
        ]


class ProductSerializer(ModelSerializer):
    """Serializer for ProductModel."""

    tags = ProductTagModelSerializer(many=True,)
    tags = ListSerializer(child=CharField(), required=False)


    class Meta(object):
        model = ProductModel
        fields = [
            'version',
            'id',
            'name',
            'price',
            'description',
            'tags',
            'product_type',
            'get_absolute_url',
            'img_url',
        ]
