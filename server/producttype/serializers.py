from rest_framework.serializers import ModelSerializer

from server.producttype.models import ProductTypeModel


class ProductTypeSerializer(ModelSerializer):
    """Serializer ProductTypeModel."""

    class Meta(object):
        """Metaclass for the ProductTypeSerializer."""

        model = ProductTypeModel
        fields = ('name', 'get_absolute_url')
