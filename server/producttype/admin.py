from django.contrib import admin

from server.producttype.models import ProductTypeModel

admin.site.register(ProductTypeModel)
