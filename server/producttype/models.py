from unittest.util import _MAX_LENGTH

from django.db.models import Model
from django.db.models.fields import CharField
from django.urls import reverse

MAX_LENGTH = 255


class ProductTypeModel(Model):
    """Model ProductTypeModel."""

    name = CharField(verbose_name='Название', max_length=MAX_LENGTH)

    class Meta(object):
        """Metaclass for the ProductTypeModel."""

        db_table = 'product_type'
        verbose_name = 'Тип продукта'
        verbose_name_plural = 'Типы продуктов'
        ordering = ('name',)

    def __str__(self):
        """This is the __str__ for the ProductTypeModel."""
        return str(self.name) if self.name is not None else 'Нет типа'

    def get_absolute_url(self):
        """This is the get_absolute_url for the ProductTypeModel."""
        return reverse('producttypemodel-detail', kwargs={'pk': self.pk})
