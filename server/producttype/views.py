from rest_framework import viewsets

from server.producttype.models import ProductTypeModel
from server.producttype.serializers import ProductTypeSerializer


class ProductTypeViewSet(viewsets.ModelViewSet):
    """View for ProductModel."""

    serializer_class = ProductTypeSerializer
    queryset = ProductTypeModel.objects.all()
