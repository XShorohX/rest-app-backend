from django.apps import AppConfig


class ProductTypeConfig(AppConfig):
    verbose_name = 'Типы продуктов'
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'server.producttype'
